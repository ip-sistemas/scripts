#!/bin/bash

DIRORIGEN="/var/spool/asterisk/monitor"
DIRDESTINO="/mnt/respaldo"
DIRREMOTO="root@201.243.68.28:/var/www/html/audio"
DIRCSV="/var/www/html/inspector_modular/cdr_export"
DBUSER="marcost15"
DBPASS="JavierT15"
DB="mbilling"
SERVER="qvox"

AYER=$(date -d "yesterday" +"%Y-%m-%d")
find "$DIRORIGEN" -size 0c -delete
umount "$DIRDESTINO"
sshfs -p 22 "$DIRREMOTO" "$DIRDESTINO"
touch "$DIRDESTINO/.lock"

for X in "$DIRORIGEN"/*; do
    CLIENTE="${X#$DIRORIGEN}"
    for Y in "$X"/*; do
        FECHAARCH=$(date -r "$Y" +"%Y-%m-%d")
        if [ "$AYER" = "$FECHAARCH" ]; then
            mkdir -p "$DIRDESTINO$CLIENTE"
            mv -n "$Y" "$DIRDESTINO$CLIENTE"
        fi
    done
done

ARCHIVOCSV="$DIRCSV/cdr-$SERVER-$(date +%F).csv"

mysql -e "
    SELECT * INTO OUTFILE '$ARCHIVOCSV' 
    FIELDS TERMINATED BY ',' 
    OPTIONALLY ENCLOSED BY '\"' 
    LINES TERMINATED BY '\n' FROM (
        SELECT 
            a.starttime, 
            DATE_SUB(CURDATE(), INTERVAL 1 DAY) AS fecha, 
            a.callerid, 
            a.calledstation, 
            a.sessiontime, 
            a.src, 
            a.terminatecauseid
    FROM pkg_cdr a 
	WHERE a.starttime LIKE CONCAT(DATE_SUB(CURDATE(), INTERVAL 1 DAY), \" %\") 
UNION
	SELECT
        b.starttime, 
        DATE_SUB(CURDATE(), INTERVAL 1 DAY) AS fecha, 
        b.callerid, 
        b.calledstation, 
        0 as sessiontime, 
        b.src, 
        b.terminatecauseid
    FROM pkg_cdr_failed b
	WHERE b.starttime LIKE CONCAT(DATE_SUB(CURDATE(), INTERVAL 1 DAY), \" %\")
) c
" -u "$DBUSER" -p "$DBPASS" "$DB";

mv "$DIRCSV"/*.csv "$DIRDESTINO"
rm "$DIRDESTINO/.lock"
umount "$DIRDESTINO"
