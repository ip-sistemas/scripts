#!/usr/bin/env php
<?php

function validaFecha($date, $format = 'Y-m-d')
{
	$d = DateTime::createFromFormat($format, $date);
	return $d && $d->format($format) === $date;
}

if (isset($argv[1]) and validaFecha($argv[1])) {
	define('FECHA', $argv[1]);
} else {
	define('FECHA', date('Y-m-d', strtotime("-1 days")));
}


define('CFG', parse_ini_file('/var/www/html/inspector/private/config.php'));
define('URL_AUDIOS', 'https://3valcentral.online/monitor/');
define('CARPETA_AUDIOS', '/home/arnoldobr/Descargas/audio/'.FECHA);


// Checking whether file exists or not
if (!file_exists(CARPETA_AUDIOS)) {
 
    // Create a new file or direcotry
    mkdir(CARPETA_AUDIOS, 0777, true);
}


// Initialize a CURL session.
$ch = curl_init();

curl_setopt_array($ch, array(
	CURLOPT_URL => URL_AUDIOS,
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_SSL_VERIFYPEER => false,
	CURLOPT_SSL_VERIFYHOST => false,
	CURLOPT_HEADER => 0,
));

$result = curl_exec($ch);
curl_close($ch);
preg_match_all('/\[DIR\]"><\/td><td><a href="([^"]+)/', $result, $directorios);
array_shift($directorios);
foreach ($directorios[0] as $midir) {
	echo "Procesando el directorio $midir:::\n";
	$ch = curl_init();

	curl_setopt_array($ch, array(
		CURLOPT_URL => URL_AUDIOS . DIRECTORY_SEPARATOR . $midir,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_SSL_VERIFYHOST => false,
		CURLOPT_HEADER => 0,
	));
	$result = curl_exec($ch);
	curl_close($ch);
	$regex = '/\]"><\/td><td><a href="([^"]+).+' . FECHA . '/';
	preg_match_all($regex, $result, $archivos);
	array_shift($archivos);
	print_r($archivos);
	foreach ($archivos[0] as $miarchivo) {
		$miurl = URL_AUDIOS . DIRECTORY_SEPARATOR . $midir . $miarchivo;
		$audiogsm = file_put_contents(CARPETA_AUDIOS . DIRECTORY_SEPARATOR . $miarchivo, file_get_contents($miurl));
	}
}
