El archivo csv se puede obtener con esta instrucción mysql

SELECT 
    calledstation
FROM
    mbilling
INTO OUTFILE '/tmp/calledstation.csv' 
FIELDS ENCLOSED BY '"' 
TERMINATED BY ';' 
ESCAPED BY '"' 
LINES TERMINATED BY '\r\n';

y luego se puede mover a cualquier directorio así:

```bash
   mv /tmp/calledstation.csv ~/directorio/ 
```
o se copia: 
```bash
   cp /tmp/calledstation.csv ~/directorio/ 
```
