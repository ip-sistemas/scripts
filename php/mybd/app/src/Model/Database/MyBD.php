<?php

namespace LR\Model\Database;

use mysqli;
use Exception;

class MyBD
{
	private mysqli $conexion;

	public function __construct(
		private string $host = "localhost",
		private string $bd = "myDataBaseName",
		private string $user = "root",
		private string $password = ""
	) {
		$this->conexion = new mysqli($host, $user, $password, $bd);
		if (mysqli_connect_errno()) {
			throw new Exception("No se pudo conectar a la base de datos");
		}
	}

	public function ejecutarQuery(string $query, array $params = []): bool|array
	{
		$stmt = $this->conexion->prepare($query);
		if ($params) {
			$stmt->bind_param(...$params);
		}
		$stmt->execute();
		$result = $stmt->get_result();
		return $result ? $result->fetch_all(MYSQLI_ASSOC) : true;
	}

	public function cerrar(): void
	{
		$this->conexion->close();
	}
}
