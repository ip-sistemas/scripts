# Envoltura para mysqli que cumple con psr-4

Si no tiene el archivo `composer.json` en la raiz de su proyecto, debe crearlo.

Debe agregarle 
```json
{
    "autoload": {
        "psr-4": {"LR\\": "src"}
    },
}
```
a su archivo json. Luego debe ejecutar
```bash
composer dump-autoload
```
que creará o modificará el archivo autoloader.php que se encuentra en la carpeta vendor.

Videos de ayuda disponibles: 

https://www.youtube.com/watch?v=qQA3P72M9-Y&t=75s 

Si no quieres usar composer, entonces... buscate la vida... Hay tutoriales sobre como implementar psr-4 sin composer.

Ahora debes crear la carpeta src en la raiz de tu app. y dentro de ella van las clases
```console
src/
    Model/
        Database/
        Tables/
        Traits/
```

Las clases que están en cada carpeta tendrán un namespace específico. Por ejemplo, La clase 
`MyBD` se creará en el archivo `src/Model/Database/MyBD.php` y comenzará así:
```php
<?php
namespace LR\Model\Database;

use mysqli;
use Exception;

class MyBD
{
/*[...]*/
}
```

Y Applog que contiene el crud de la tabla applogs y otros métodos:
```php
<?php

namespace LR\Model\Tables;

use LR\Model\Database\MyBD; 
use LR\Model\Traits\CRUDMethods;

class Applog
{
    use CRUDMethods;

    public function __construct(MyBD $bd)
    {
        $this->bd = $bd;
        $this->tabla = 'applogs';
    }
/*[...]*/
}
```

En la carpeta src se pueden tener otras clases tanto sueltas como en carpetas.
