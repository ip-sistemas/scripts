#!/bin/bash

ARCHIVO=/var/www/html/inspector_modular/cdr_export/cdr-qvox-$(date +%F).csv

mysql -e "select * 
into outfile '$ARCHIVO'
FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '\"'
LINES TERMINATED BY '\n'
from (
	select 
    	a.starttime,DATE_SUB(CURDATE(), INTERVAL 1 DAY) AS fecha , a.callerid , 
    	a.calledstation , a.sessiontime , a.src, a.terminatecauseid 
     	from pkg_cdr a 
	where a.starttime LIKE CONCAT(DATE_SUB(CURDATE(), INTERVAL 1 DAY), \" %\")
union
	select 
    	b.starttime,DATE_SUB(CURDATE(), INTERVAL 1 DAY) AS fecha , b.callerid , 
    	b.calledstation , 0 as sessiontime , b.src, b.terminatecauseid
     	from pkg_cdr_failed b
	where b.starttime LIKE CONCAT(DATE_SUB(CURDATE(), INTERVAL 1 DAY), \" %\")
) c
" -umarcost15 -pJavierT15 mbilling;
