#!/bin/bash

D0=2022-07-20
DF=2022-09-14

while ! [[ $D0  >  $DF ]]; do
	echo Procesando $D0
	echo '========================='
	/home/marcost15/historico/historico.php -d $D0
	D0=$(date -I -d "$D0 + 1 day")
done
