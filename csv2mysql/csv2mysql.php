#!/usr/bin/env php
<?php

function procesar_archivo($archivo_entrada, $usuarios, $archivo_salida)
{
	$max_tuplas = 500;
	$presql = "INSERT INTO pkg_restrict_phone (id, id_user, number, direction) VALUES ";
	$tuplas = '';
	$i = 0;
	$lineas = array_unique(file($archivo_entrada, FILE_IGNORE_NEW_LINES));
	$salida = fopen($archivo_salida, 'w');
	$usuarios = explode(',', $usuarios);
	foreach ($lineas as $numero) {
		$numero = str_replace(['"', ' '], '', $numero);
		foreach ($usuarios as $user) {
			$tupla = "(NULL, '$user', '$numero', '1')";
			if ($i === 0) {
				$tuplas = $tupla;
			} else {
				$tuplas .= ", $tupla";
			}
			$i++;
		}
		if ($i > $max_tuplas) {
			fwrite($salida, "$presql $tuplas;\n");
			$tuplas = '';
			$i = 0;
		}
	}
	if ($tuplas !== '') {
		fwrite($salida, "$presql $tuplas;\n");
	}
	fclose($salida);
	return count($lineas);
}

function mostrar_ayuda()
{
	echo <<<EOF
Uso: {$_SERVER['PHP_SELF']} archivo_entrada usuarios archivo_salida
	archivo_entrada es el nombre del archivo que se va a procesar. 
	                Este archivo es una lista de números telefónicos uno por línea.
	usuarios        es una lista de números separada por comas de los usuarios 
	                a quienes se le agregará la lista de telefonos. 
	                Por ejemplo: 3,45,354
    archivo_salida  es el nombre del archivo de salida
EOF;
}

if ($argc !== 4) {
	mostrar_ayuda();
	exit(1);
}

$archivo_entrada = $argv[1];
$usuarios = $argv[2];
$archivo_salida = $argv[3];

if (!is_readable($archivo_entrada)) {
	echo "Error: no se puede leer el archivo de entrada '$archivo_entrada'\n";
	exit(1);
}

if (file_exists($archivo_salida) && !is_writable($archivo_salida)) {
	echo "Error: no se puede escribir en el archivo de salida '$archivo_salida'\n";
	exit(1);
}

$numero_lineas = procesar_archivo($archivo_entrada, $usuarios, $archivo_salida);
echo "Cantidad de números procesados: $numero_lineas\n";
echo "[LISTO]\n";
echo "Archivo generado: '$archivo_salida'\n";
