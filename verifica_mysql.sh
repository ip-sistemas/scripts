#!/bin/bash
# Solo root puede ejecutar este código
if [[ $EUID -ne 0 ]]; then
    echo "Este script debe ser ejecutado como root."
    exit 1
fi
# Verificar si MySQL está bloqueado o no se está ejecutando
if ! mysqladmin ping &>/dev/null; then
    echo "MySQL está bloqueado o no se está ejecutando. Reiniciando la máquina..."
    reboot
    # shutdown -r now
fi
