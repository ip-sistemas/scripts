#!/bin/bash

D0=2022-07-19
DF=2022-09-04

while ! [[ $D0  >  $DF ]]; do
	echo Procesando $D0
	/home/marcost15/inspector/inspector.php -d $D0
	D0=$(date -I -d "$D0 + 1 day")
done
