#!/bin/bash

# Las tareas de cron creadas estan en /var/spool/cron


for user in $(cut -f1 -d: /etc/passwd)
do
	echo "---------------------- {{ $user }}"
	sudo crontab -u $user -l |grep -v -e "^no"|grep -v -e "^\#"
done
