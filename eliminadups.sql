CREATE
TEMPORARY TABLE temp_cdr
SELECT DISTINCT uniqueid,
                calledstation
FROM pkg_cdr_h;


DELETE
FROM pkg_cdr_h
WHERE (uniqueid,
       sessiontime,
       calledstation) NOT IN
        (SELECT uniqueid,
                sessiontime,
                calledstation
         FROM temp_cdr);


DROP TABLE temp_cdr;


CREATE
TEMPORARY TABLE temp_cdr_failed
SELECT DISTINCT uniqueid,
                calledstation
FROM pkg_cdr_failed_h;


DELETE
FROM pkg_cdr_failed_h
WHERE (uniqueid,
       sessiontime,
       calledstation) NOT IN
        (SELECT uniqueid,
                calledstation
         FROM temp_cdr_failed);


DROP TABLE temp_cdr_failed;
