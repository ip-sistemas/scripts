#!/bin/bash

read -p "Base de Datos: " bd
read -p "Usuario: " usuario
read -s -p "Clave: " clave

mkdir -p $bd

mysql --user=$usuario --password=$clave $bd -e "SHOW TABLES" | tail -n +2 > tablas.txt

for x in $(cat tablas.txt)
do
  echo Procesando $bd .. $x
  mysqldump --user=$usuario --password=$clave $bd $x > $bd/$x.sql
done
rm tablas.txt
