<?php

namespace LR\Model;

use LR\Database\MyBD;
use LR\Traits\CRUDMethods;

class Applog
{
	use CRUDMethods;

	public function __construct(MyBD $bd)
	{
		$this->bd = $bd;
		$this->tabla = 'applogs';
	}

	public function crear(string $usuarioId, string $eventoTipo, string $descripcion): void
	{
		$this->crearRegistro([
			'ip' => $_SERVER["REMOTE_ADDR"],
			'user_id' => $usuarioId,
			'sesion_id' => session_id(),
			'agente_usuario' => $_SERVER['HTTP_USER_AGENT'],
			'tipo_evento' => $eventoTipo,
			'detalle' => $descripcion,
		]);
	}
}
