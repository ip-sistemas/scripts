<?php

namespace LR\Traits;

trait CRUDMethods
{
    protected \LR\Database\MyBD $bd;
    protected string $tabla;

    public function getTabla()
    {
        return $this->tabla;
    }

    public function crearRegistro(array $data): bool
    {
        $campos = implode(", ", array_keys($data));
        $placeholders = implode(", ", array_fill(0, count($data), "?"));
        $query = "INSERT INTO {$this->tabla} ($campos) VALUES ($placeholders)";
        $params = [str_repeat("s", count($data)), ...array_values($data)];
        return $this->bd->ejecutarQuery($query, $params);
    }

    public function leerRegistro(int $id): array
    {
        $query = "SELECT * FROM {$this->tabla} WHERE id = ?";
        return $this->bd->ejecutarQuery($query, ["i", $id])[0] ?? [];
    }

    public function actualizarRegistro(int $id, array $data): bool
    {
        $campos = implode(", ", array_map(fn($key) => "$key = ?", array_keys($data)));
        $query = "UPDATE {$this->tabla} SET $campos WHERE id = ?";
        $params = [str_repeat("s", count($data)) . "i", ...array_values($data), $id];
        return $this->bd->ejecutarQuery($query, $params);
    }

    public function eliminarRegistro(int $id): bool
    {
        $query = "DELETE FROM {$this->tabla} WHERE id = ?";
        return $this->bd->ejecutarQuery($query, ["i", $id]);
    }

    public function contarFilas(string $condicion = '', array $params = []): int
    {
        $query = "SELECT COUNT(*) as total FROM {$this->tabla}";
        if ($condicion) {
            $query .= " WHERE $condicion";
        }
        $resultado = $this->bd->ejecutarQuery($query, $params);
        return $resultado[0]['total'] ?? 0;
    }

    public function read($id, $columnas = [], $criterio = []): array
    {
        $columnasSeleccionadas = $columnas ? implode(", ", $columnas) : '*';
        $query = "SELECT $columnasSeleccionadas FROM {$this->tabla} WHERE id = ?";
        $params = ["i", $id];

        if ($criterio) {
            foreach ($criterio as $key => $value) {
                $query .= " AND $key = ?";
                $params[0] .= "s";
                $params[] = $value;
            }
        }

        $resultado = $this->bd->ejecutarQuery($query, $params);
        return $resultado[0] ?? [];
    }

    public function sql2array(string $query, array $params = []): array
    {
        return $this->bd->ejecutarQuery($query, $params);
    }

    public function sql2options(string $query, string $keyField, string $valueField, array $params = []): array
    {
        $result = $this->bd->ejecutarQuery($query, $params);
        $options = [];
        foreach ($result as $row) {
            $options[$row[$keyField]] = $row[$valueField];
        }
        return $options;
    }

    public function sql2row(string $query, array $params = []): array
    {
        $result = $this->bd->ejecutarQuery($query, $params);
        return $result[0] ?? [];
    }

    public function sql2value(string $query, array $params = [])
    {
        $result = $this->bd->ejecutarQuery($query, $params);
        return $result[0] ? array_values($result[0])[0] : null;
    }
}
