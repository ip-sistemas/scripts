#CREATE USER 'arnoldobr'@'%' IDENTIFIED BY 'clave';
#CREATE USER 'arnoldobr'@'localhost' IDENTIFIED BY 'clave';
GRANT ALL PRIVILEGES ON *.* TO 'arnoldobr'@'localhost' WITH GRANT OPTION IDENTIFIED BY 'clave';
GRANT ALL PRIVILEGES ON *.* TO 'arnoldobr'@'%' WITH GRANT OPTION IDENTIFIED BY 'clave';
FLUSH PRIVILEGES;
