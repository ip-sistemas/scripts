#!/bin/bash
#IF gsm.convert SCRIPT IS ALLREADY RUNNING THEN TERMINATE AND RUN LATER

# FILE="$HOME/Download/gsm-convert.lock"

# if [ -f "$FILE" ]; then
#    echo "Still running gsm-convert script.  Exiting..."
#   rm -f $FILE
#   sleep 5
#   exit
#fi

# touch $FILE

#IF THERE IS AN ACTIVE DOWNLOAD OF A .gsm FILE DO NOT CONVERT YET

# count=`ls -1 $HOME/Download/*.part 2>/dev/null | wc -l`
# if [ $count != 0 ]; then
#   echo ".part files exist. Exiting conversion script..."
#   rm -f $FILE
#   sleep 5
#   exit
# fi

#CONVERT ALL .gsm FILES IN USERS Download FOLDER TO .mp3 AND DELETE THE .gsm
# DIR=/var/www/html/audio
DIR=$HOME/public_html/git/inspector/otros/clasificacion/audio

LIST=$(ls $DIR/*/*.gsm)
for i in $LIST; do
   DEST=$(ls $i | cut -d "." -f 1,2).mp3
   lame -V 9 $i $DEST
   echo "done converting $i, removing.. $DEST"
   rm $i
done;
#rm -f $FILE