#!/bin/bash

declare -a C=( 
"uname -a"
"lsb_release -a"
"cat /etc/debian_version"
"cat /etc/issue"
"uptime"
"hostname"
"hostname -i"
"last reboot"
"cat /proc/cpuinfo"
"cat /proc/meminfo"
"grep MemTotal /proc/meminfo"
"watch -n1 'cat /proc/interrupts'"
"free -m"
"cat /proc/devices"
"lspci -tv"
"lsusb -tv"
"lshal"
"dmidecode"
"dd if=/dev/mem bs=1k skip=768 count=256 2>/dev/null | strings -n 8"
)

FIN=${#C[*]}


for X in $(seq 1 $FIN)
do
	echo "============================================================================="
	echo ${C[$X]}
	echo "· · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · · "
	${C[$X]}
	echo ''
done
