#!/usr/bin/env php
<?php

include_once 'php/myBD/myBD.php';
include_once 'php/debug/debug.php';

$numeros = [];

/**
 * devuelve la cantidad de telefonos que cumple con los parámetros dados de acd y asr
 */
function num_telefonos($seg_ACD, $porc_ASR, $numeros)
{

	$aprobados = [];

	foreach ($numeros as $numero) {
		($numero['ACD'] > $seg_ACD) and ($numero['ASR'] > $porc_ASR) and ($aprobados[] = $numero);
	}
	return count($aprobados);
}


$bd = new myBD('localhost', 'inspector', 'arnoldobr', 'ozemroland');

$numeros = $bd->sql2array(
	"
	SELECT calledstation, count(*) total_llamadas, sum(sessiontime) total_seg, sum(sessiontime=0) no_conectadas
	FROM consolidado
	WHERE calledstation IN (SELECT DISTINCT calledstation WHERE calledstation LIKE '58412%') 
	GROUP BY calledstation
	ORDER BY calledstation ASC;"
);

foreach ($numeros as &$numero) {
	$numero['conectadas'] = $numero['total_llamadas'] - $numero['no_conectadas'];
	if ($numero['conectadas'] == 0) {
		$numero['ACD'] = 0;
		$numero['ASR'] = 0.0;
	} else {
		$numero['ACD'] = $numero['total_seg'] / $numero['conectadas'];
		$numero['ASR'] = $numero['conectadas'] * 100.0 / $numero['total_llamadas'];
	}
}


// Para graficar
// echo "ACD, ASR, Cantidad\n";

// for ($seg_ACD = 60; $seg_ACD <= 3600; $seg_ACD += 30) {
// 	for ($porc_ASR = 1; $porc_ASR <= 100; $porc_ASR += 1) {
// 		echo $seg_ACD, ', ', $porc_ASR, ', ', num_telefonos($seg_ACD, $porc_ASR, $numeros), "\n";
// 	}
// }

foreach ($numeros as $numero) {
	if ($numero['ACD'] >= 180 and $numero['ASR'] >= 15.0){
		echo "{$numero['calledstation']}\n";
	} 
}
