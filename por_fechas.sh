#!/bin/bash

function mostrar_ayuda{
    echo "Uso: $0 [OPCIONES] FECHA0 FECHA1 COMANDO"
    echo
    echo "Realiza una tarea para cada día entre las fechas especificadas."
    echo "El comando a ejecutar puede tener opciones y parámetros, pero debe aceptar"
    echo "un parámetro -d que tomará la fecha y ejecutará el proceso." 
    echo "Por ejemplo, $0 -v 2022-01-01 2023-01-01 comando -o asdfds -p asdf"
    echo "ejecutará"
    echo "  comando -o asdfds -p asdf -d 2022-01-01"
    echo "  comando -o asdfds -p asdf -d 2022-01-02"
    echo "y así hasta 2023-01-01"
    echo
    echo "Opciones:"
    echo "  -h, --help       muestra este mensaje de ayuda"
    echo "  -v, --verbose    muestra información adicional"
    exit 0
}

if [ "$#" -lt 3 ]; then
    echo "Error: se requieren al menos tres argumentos (FECHA0, FECHA1 y COMANDO)." >&2
    exit 1
fi


# Procesar las opciones
VERBOSE=0
while [ "$#" -gt 0 ]; do
    case "$1" in
        -h|--help)
            mostrar_ayuda
            ;;
        -v|--verbose)
            VERBOSE=1
            shift
            ;;
        -*)
            echo "Error: opción inválida: $1" >&2
            exit 1
            ;;
        *)
            break
            ;;
    esac
done

FECHA0=$1
FECHA1=$2

# Verificar que las fechas estén en el formato correcto
if ! date -f "%Y-%m-%d" "$FECHA0" > /dev/null 2>&1; then
    echo "Error: la fecha $FECHA0 no está en el formato YYYY-MM-DD." >&2
    exit 1
fi
if ! date -f "%Y-%m-%d" "$FECHA1" > /dev/null 2>&1; then
    echo "Error: la fecha $FECHA1 no está en el formato YYYY-MM-DD." >&2
    exit 1
fi


COMANDO="$3"
shift 3
COMANDO="$COMANDO $@"

while ! [[ $FECHA0 > $FECHA1 ]]; do
    $COMANDO -d $FECHA0
    FECHA0=$(date -d "$FECHA0 + 1 day" +%F)
done
