#!/bin/bash
# 
# Este script agrega o elimina a  restrict_phone los 
# los números que se encuentren en el array listanegra a una hora determinada
# y deben desaparecer de la misma lista a otra hora dada
# 

# Lista de ID_USER
ID_USERS=("17")  # Agrega los ID_USER que necesites ("17" "3" "4")

# Lista de números
NUMEROS=("58")  # Agrega los números que necesites ("58" "59" "60" "61")

# Configuración de la conexión a la base de datos
DB_USER="marcost15"
DB_PASSWORD="JavierT15"
DB_NAME="mbilling"
DB_HOST="localhost"  # Puedes cambiarlo según tu configuración

# Función para ejecutar la consulta SQL
ejecutar_sql() {
  local ACCION="$1"
  local NUMERO="$2"

  for ID_USER in "${ID_USERS[@]}"; do
    local SQL_COMMAND=""
    # Establecer el comando SQL basado en la acción
    if [ "$ACCION" == "insert" ]; then
      SQL_COMMAND="INSERT IGNORE INTO pkg_restrict_phone (id, id_user, number, direction) VALUES (NULL,'$ID_USER','$NUMERO',1);"
    elif [ "$ACCION" == "delete" ]; then
      SQL_COMMAND="DELETE FROM pkg_restrict_phone WHERE number = '$NUMERO' AND id_user = '$ID_USER';"
    else
      echo "Comando no reconocido. Debe ser 'insert' o 'delete'."
      exit 1
    fi

    # Ejecutar el comando SQL
    mysql -u "$DB_USER" -p"$DB_PASSWORD" -h "$DB_HOST" -D "$DB_NAME" -e "$SQL_COMMAND"
  done
}

# Verificar si se proporciona un número suficiente de argumentos
if [ "$#" -ne 1 ]; then
  echo "Uso: $0 <accion>"
  exit 1
fi

# Llamar a la función con los parámetros proporcionados
for NUMERO in "${NUMEROS[@]}"; do
  ejecutar_sql "$1" "$NUMERO"
done